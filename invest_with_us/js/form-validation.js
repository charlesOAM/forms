var fieldIds = [
  "companyInvest",
  "emailInvest",
  "firstNameInvest",
  "lastNameInvest",
  "phoneInvest",
];

var errorIds = [
  "companyInvestError",
  "emailInvestError",
  "firstNameInvestError",
  "lastNameInvestError",
  "notWorkEmailInvestError",
  "phoneInvestError",
  "validEmailInvestError",
];

var emailDomainFilter = ["@hotmail", "@yahoo", "@aol", "@gmail"];

function validateEmailInvest(email) {
  return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);
}

function validateWorkEmailInvest(email) {
  var isWorkEmail = true;
  emailDomainFilter.forEach(function(domain) {
    if(email.indexOf(domain) > 0) {
      isWorkEmail = false;
    }
  })
  return isWorkEmail;
}

function clearErrorsInvest() {
  fieldIds.forEach(function(field) {
    document.getElementById(field).classList.remove('has-error')
  });
}

function clearErrorMessagesInvest() {
  errorIds.forEach(function(fieldError) {
    document.getElementById(fieldError).style.display = 'none';
  })
}

function validateFormInvest() {
  var isFormValid = true;
  var company = document.forms["investForm"]["fields[company]"].value;
  var lastName = document.forms["investForm"]["fields[last_name]"].value;
  var phone = document.forms["investForm"]["fields[phone]"].value;
  var email = document.forms["investForm"]["fields[email]"].value;
  var firstName = document.forms["investForm"]["fields[first_name]"].value;
  var isValidEmail = validateEmailInvest(email);
  var isWorkEmail = validateWorkEmailInvest(email);

  clearErrorsInvest();
  clearErrorMessagesInvest();

  if(!firstName) {
    document.getElementById("firstNameInvest").classList.add('has-error');
    document.getElementById("firstNameInvestError").style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById("firstNameInvest").classList.remove('has-error');
    document.getElementById("firstNameInvestError").style.display = 'none';
  }

  if(!lastName) {
    document.getElementById("lastNameInvest").classList.add('has-error')
    document.getElementById("lastNameInvestError").style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById("lastNameInvest").classList.remove('has-error')
    document.getElementById("lastNameInvestError").style.display = 'none';
  }

  if(!email) {
    document.getElementById("emailInvest").classList.add('has-error')
    document.getElementById("emailInvestError").style.display = 'block';
    isFormValid = false;
  } else if (email && !isValidEmail || email && !isWorkEmail){
    document.getElementById("emailInvest").classList.add('has-error')
    document.getElementById("emailInvestError").style.display = 'none';
  } else {
    document.getElementById("emailInvest").classList.remove('has-error')
  }

  if(email && !isValidEmail) {
    document.getElementById("emailInvest").classList.add('has-error')
    document.getElementById("validEmailInvestError").style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById("validEmailInvestError").style.display = 'none';
  }

  if(email && !isWorkEmail) {
    document.getElementById("emailInvest").classList.add('has-error')
    document.getElementById("notWorkEmailInvestError").style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById("notWorkEmailInvestError").style.display = 'none';
  }

  if(!company) {
    document.getElementById("companyInvest").classList.add('has-error')
    document.getElementById("companyInvestError").style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById("companyInvest").classList.remove('has-error')
    document.getElementById("companyInvestError").style.display = 'none';
  }

  if(!phone) {
    document.getElementById("phoneInvest").classList.add('has-error')
    document.getElementById("phoneInvestError").style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById("phoneInvest").classList.remove('has-error')
    document.getElementById("phoneInvestError").style.display = 'none';
  }

  return isFormValid
}
