function submitInvestForm(event) {
  event.preventDefault();
	var elts =  document.querySelector('.invest-form');

	// validation ... are required fields filled out?
	if (!validateFormInvest()) {
		return false;
	}


	var investForm = document.getElementById('invest-form');
  investForm.action="https://www.getdrip.com/forms/67247858/submissions";
	investForm.method = "post";
  investForm.submit();
}
