var fieldIds = [
  'certifiedProfessional',
  'company',
  'country',
  'email',
  'firstName',
  'lastName',
  'phone',
  'state',
  'zipcode',
  'mailingList'
];

var errorIds = [
  'certifiedProfessionalError',
  'companyError',
  'countryError',
  'emailError',
  'firstNameError',
  'lastNameError',
  'notWorkEmailError',
  'phoneError',
  'stateError',
  'validEmailError',
  'zipcodeError',
  'mailingListError'
];

var emailDomainFilter = ['@hotmail', '@yahoo', '@aol', '@gmail'];

function validateEmail(email) {
  return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);
}

function validateWorkEmail(email) {
  var isWorkEmail = true;
  emailDomainFilter.forEach(function(domain) {
    if (email.indexOf(domain) > 0) {
      isWorkEmail = false;
    }
  });
  return isWorkEmail;
}

function clearErrors() {
  fieldIds.forEach(function(field) {
    document.getElementById(field).classList.remove('has-error');
  });
}

function clearErrorMessages() {
  errorIds.forEach(function(fieldError) {
    document.getElementById(fieldError).style.display = 'none';
  });
}

function validateForm() {
  var isFormValid = true;
  var certifiedCheckBox = document.getElementById('certifiedProfessionalCB')
    .checked;
  var company = document.forms['faSignUp']['fields[company]'].value;
  var country = document.forms['faSignUp']['fields[country]'].value;
  var email = document.forms['faSignUp']['fields[email]'].value;
  var firstName = document.forms['faSignUp']['fields[first_name]'].value;
  var isValidEmail = validateEmail(email);
  var isWorkEmail = validateWorkEmail(email);
  var lastName = document.forms['faSignUp']['fields[last_name]'].value;
  var phone = document.forms['faSignUp']['fields[phone]'].value;
  var state = document.forms['faSignUp']['fields[state_province]'].value;
  var zipcode = document.forms['faSignUp']['fields[zipcode]'].value;
  var mailingList =
    document.getElementById('mailingList_Yes').checked == true ||
    document.getElementById('mailingList_No').checked == true;

  clearErrors();
  clearErrorMessages();

  if (!mailingList) {
    document.getElementById('mailingList').classList.add('has-error');
    document.getElementById('mailingListError').style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById('mailingList').classList.remove('has-error');
    document.getElementById('mailingListError').style.display = 'none';
  }

  if (!firstName) {
    document.getElementById('firstName').classList.add('has-error');
    document.getElementById('firstNameError').style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById('firstName').classList.remove('has-error');
    document.getElementById('firstNameError').style.display = 'none';
  }

  if (!lastName) {
    document.getElementById('lastName').classList.add('has-error');
    document.getElementById('lastNameError').style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById('lastName').classList.remove('has-error');
    document.getElementById('lastNameError').style.display = 'none';
  }

  if (!email) {
    document.getElementById('email').classList.add('has-error');
    document.getElementById('emailError').style.display = 'block';
    isFormValid = false;
  } else if ((email && !isValidEmail) || (email && !isWorkEmail)) {
    document.getElementById('email').classList.add('has-error');
    document.getElementById('emailError').style.display = 'none';
  } else {
    document.getElementById('email').classList.remove('has-error');
  }

  if (email && !isValidEmail) {
    document.getElementById('email').classList.add('has-error');
    document.getElementById('validEmailError').style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById('validEmailError').style.display = 'none';
  }

  if (email && !isWorkEmail) {
    document.getElementById('email').classList.add('has-error');
    document.getElementById('notWorkEmailError').style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById('notWorkEmailError').style.display = 'none';
  }

  if (!company) {
    document.getElementById('company').classList.add('has-error');
    document.getElementById('companyError').style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById('company').classList.remove('has-error');
    document.getElementById('companyError').style.display = 'none';
  }

  if (!state) {
    document.getElementById('state').classList.add('has-error');
    document.getElementById('stateError').style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById('state').classList.remove('has-error');
    document.getElementById('stateError').style.display = 'none';
  }

  if (!phone) {
    document.getElementById('phone').classList.add('has-error');
    document.getElementById('phoneError').style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById('phone').classList.remove('has-error');
    document.getElementById('phoneError').style.display = 'none';
  }

  if (!country) {
    document.getElementById('country').classList.add('has-error');
    document.getElementById('countryError').style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById('country').classList.remove('has-error');
    document.getElementById('countryError').style.display = 'none';
  }

  if (!zipcode) {
    document.getElementById('zipcode').classList.add('has-error');
    document.getElementById('zipcodeError').style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById('zipcode').classList.remove('has-error');
    document.getElementById('zipcodeError').style.display = 'none';
  }

  if (!certifiedCheckBox) {
    document.getElementById('certifiedProfessional').classList.add('has-error');
    document.getElementById('certifiedProfessionalError').style.display =
      'block';
    isFormValid = false;
  } else {
    document
      .getElementById('certifiedProfessional')
      .classList.remove('has-error');
    document.getElementById('certifiedProfessionalError').style.display =
      'none';
  }

  return isFormValid;
}
