function submitContactForm(event) {
  event.preventDefault();
	var elts =  document.querySelector('.contact-form');

	// validation ... are required fields filled out?
	if (!validateForm()) {
		return false;
	}


	var contactForm = document.getElementById('contact-form');
  contactForm.action="https://www.getdrip.com/forms/75452850/submissions";
	contactForm.method = "post";
  contactForm.submit();
}
