var fieldIds = [
  "company",
  "email",
  "firstName",
  "lastName",
  "message",
  "phone"
];

var errorIds = [
  "companyError",
  "emailError",
  "firstNameError",
  "lastNameError",
  "messageError",
  "notWorkEmailError",
  "phoneError",
  "validEmailError",
];

var emailDomainFilter = ["@hotmail", "@yahoo", "@aol", "@gmail"];

function validateEmail(email) {
  return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);
}

function validateWorkEmail(email) {
  var isWorkEmail = true;
  emailDomainFilter.forEach(function(domain) {
    if(email.indexOf(domain) > 0) {
      isWorkEmail = false;
    }
  })
  return isWorkEmail;
}

function clearErrors() {
  fieldIds.forEach(function(field) {
    document.getElementById(field).classList.remove('has-error')
  });
}

function clearErrorMessages() {
  errorIds.forEach(function(fieldError) {
    document.getElementById(fieldError).style.display = 'none';
  })
}

function validateForm() {
  var isFormValid = true;
  var company = document.forms["contactForm"]["fields[company]"].value;
  var email = document.forms["contactForm"]["fields[email]"].value;
  var firstName = document.forms["contactForm"]["fields[first_name]"].value;
  var isValidEmail = validateEmail(email);
  var isWorkEmail = validateWorkEmail(email);
  var lastName = document.forms["contactForm"]["fields[last_name]"].value;
  var phone = document.forms["contactForm"]["fields[phone]"].value;
  var message = document.forms["contactForm"]["fields[message]"].value;

  clearErrors();
  clearErrorMessages();

  if(!firstName) {
    document.getElementById("firstName").classList.add('has-error');
    document.getElementById("firstNameError").style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById("firstName").classList.remove('has-error');
    document.getElementById("firstNameError").style.display = 'none';
  }

  if(!lastName) {
    document.getElementById("lastName").classList.add('has-error')
    document.getElementById("lastNameError").style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById("lastName").classList.remove('has-error')
    document.getElementById("lastNameError").style.display = 'none';
  }

  if(!email) {
    document.getElementById("email").classList.add('has-error')
    document.getElementById("emailError").style.display = 'block';
    isFormValid = false;
  } else if (email && !isValidEmail || email && !isWorkEmail){
    document.getElementById("email").classList.add('has-error')
    document.getElementById("emailError").style.display = 'none';
  } else {
    document.getElementById("email").classList.remove('has-error')
  }

  if(email && !isValidEmail) {
    document.getElementById("email").classList.add('has-error')
    document.getElementById("validEmailError").style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById("validEmailError").style.display = 'none';
  }

  if(email && !isWorkEmail) {
    document.getElementById("email").classList.add('has-error')
    document.getElementById("notWorkEmailError").style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById("notWorkEmailError").style.display = 'none';
  }

  if(!company) {
    document.getElementById("company").classList.add('has-error')
    document.getElementById("companyError").style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById("company").classList.remove('has-error')
    document.getElementById("companyError").style.display = 'none';
  }

  if(!phone) {
    document.getElementById("phone").classList.add('has-error')
    document.getElementById("phoneError").style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById("phone").classList.remove('has-error')
    document.getElementById("phoneError").style.display = 'none';
  }

  if(!message) {
    document.getElementById("message").classList.add('has-error')
    document.getElementById("messageError").style.display = 'block';
    isFormValid = false;
  } else {
    document.getElementById("message").classList.remove('has-error')
    document.getElementById("messageError").style.display = 'none';
  }

  return isFormValid
}
