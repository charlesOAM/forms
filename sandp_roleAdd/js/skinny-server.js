// Initialize access to the Skinny Server
var config = {
  restEndpoint: "https://services.optimalam.com/oam/v1/user"
};

function displayError(e) {
  var el = document.querySelector('.fa-signup-status');
  if (el.classList) {
    el.classList.remove('has-error', 'has-success');
    el.classList.add('has-error');
  } else {
    el.className += ' ' + 'has-error';
  }

  el.innerHTML = e
}

function displayStatus(s) {
  var el = document.querySelector('.fa-signup-status');

  if (el.classList){
    el.classList.remove('has-error', 'has-success');
    el.classList.add('has-success');
  } else {
    el.className += ' ' + 'has-success';
  }

  el.innerHTML = s
}

function formToJSON(elements) {
	return [].reduce.call(elements, function(data, element) {
		var sanitized_name = element.name.replace(/[^\[]*\[(.*)\].*/, "$1");
		if (sanitized_name == 'password' || sanitized_name == 'repeat_password') return data;
		if (element.type=="checkbox") {
			sanitized_value = element.checked?element.value:false;
		} else {
			sanitized_value = element.value;
		}
		data[sanitized_name] = sanitized_value;
		return data;
	}, {});
}

function submitSignUpForm(event) {
  event.preventDefault();
	// var elts =  document.querySelector('.fa-signup-form');

	// validation ... are required fields filled out?
	if (!validateForm()) {
		return false;
	}

 //  // merge in other fields, remember to pass in password if you need to
 //  // but if you dont pass one in, we'll create one for you
 //  var postBody = {email: elts["fields[email]"].value, resetPasswordEmail: true};
 //  var formInfo = formToJSON(elts);
 //  for (var fieldName in formInfo) { postBody[fieldName] = formInfo[fieldName]; }

	// // ok - now try to reach out to the server and register
 //  // console.log("Posting ", postBody, " to: ", config.restEndpoint);
 //  displayStatus("Connecting to the authorization server ...");

 //  // PLAIN JANE JAVASCRIPT
 //  var request = new XMLHttpRequest();
 //  request.open('POST', config.restEndpoint, true);
 //  request.setRequestHeader("Content-Type", "application/json");

 //  request.onload = function() {
 //    var resp = JSON.parse(request.responseText);
 //    // console.log("response: ", resp);
 //    if (request.status >= 200 && request.status < 400) {
 //    	if (resp.error) {
 //    		displayError("ERROR: " + resp.error.code + " " + resp.error.message);
 //    		return false;
 //    	}
 //      // Success!
 //      // console.log("Success: " + resp + " sending password email");
 //      displayStatus("Saved user information, finalizing account information ...");
	// 		var faSignUpForm = document.getElementById('fa-signup');
 //      faSignUpForm.action="https://www.getdrip.com/forms/32568431/submissions";
	// 		faSignUpForm.method = "post";
 //      faSignUpForm.submit();
 //    } else {
 //      // We reached our target server, but it returned an error
 //      // console.log("Response indicates Failure: ", resp);
 //      displayError("The server returned an error, please try later.");
 //    }
 //  };

 //  request.onerror = function() {
 //    // There was a connection error of some sort
 //    displayError("The server was unreachable, please try later.");
 //  };

 //  request.send(JSON.stringify(postBody));
  var faSignUpForm = document.getElementById('fa-signup');
  faSignUpForm.action="https://www.getdrip.com/forms/32568431/submissions";
  faSignUpForm.method = "post";
  faSignUpForm.submit();

}
