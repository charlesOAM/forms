var fieldIds = [
  'companyFactor101',
  'emailFactor101',
  'firstNameFactor101',
  'lastNameFactor101',
  'phoneFactor101',
];

var errorIds = [
  'companyFactor101Error',
  'emailFactor101Error',
  'firstNameFactor101Error',
  'lastNameFactor101Error',
  'notWorkEmailFactor101Error',
  'phoneFactor101Error',
  'validEmailFactor101Error'
];

var emailDomainFilter = ['@hotmail', '@yahoo', '@aol', '@gmail'];

function validateEmailFactor101(email) {
  return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);
}

function validateWorkEmailFactor101(email) {
  var isWorkEmail = true;
  emailDomainFilter.forEach(function(domain) {
    if (email.indexOf(domain) > 0) {
      isWorkEmail = false;
    }
  });
  return isWorkEmail;
}

function clearErrorsFactor101() {
  fieldIds.forEach(function(field) {
    document.getElementById(field).classList.remove('has-error');
  });
}

function clearErrorMessagesFactor101() {
  errorIds.forEach(function(fieldError) {
    document.getElementById(fieldError).style.display = 'none';
  });
}

function validateFactor101SignupForm() {
  var isFormValid = true;
  var company = document.forms['factor101Form']['fields[company]'].value;
  var lastName = document.forms['factor101Form']['fields[last_name]'].value;
  var phone = document.forms['factor101Form']['fields[phone]'].value;
  var email = document.forms['factor101Form']['fields[email]'].value;
  var firstName = document.forms['factor101Form']['fields[first_name]'].value;
  var isValidEmail = validateEmailFactor101(email);
  var isWorkEmail = validateWorkEmailFactor101(email);

  clearErrorsFactor101();
  clearErrorMessagesFactor101();

  if (!firstName) {
    document.getElementById('firstNameFactor101').classList.add('has-error');
    document.getElementById('firstNameFactor101Error').style.display =
      'block';
    isFormValid = false;
  } else {
    document.getElementById('firstNameFactor101').classList.remove('has-error');
    document.getElementById('firstNameFactor101Error').style.display =
      'none';
  }

  if (!lastName) {
    document.getElementById('lastNameFactor101').classList.add('has-error');
    document.getElementById('lastNameFactor101Error').style.display =
      'block';
    isFormValid = false;
  } else {
    document.getElementById('lastNameFactor101').classList.remove('has-error');
    document.getElementById('lastNameFactor101Error').style.display =
      'none';
  }

  if (!email) {
    document.getElementById('emailFactor101').classList.add('has-error');
    document.getElementById('emailFactor101Error').style.display =
      'block';
    isFormValid = false;
  } else if ((email && !isValidEmail) || (email && !isWorkEmail)) {
    document.getElementById('emailFactor101').classList.add('has-error');
    document.getElementById('emailFactor101Error').style.display =
      'none';
  } else {
    document.getElementById('emailFactor101').classList.remove('has-error');
  }

  if (email && !isValidEmail) {
    document.getElementById('emailFactor101').classList.add('has-error');
    document.getElementById('validEmailFactor101Error').style.display =
      'block';
    isFormValid = false;
  } else {
    document.getElementById('validEmailFactor101Error').style.display =
      'none';
  }

  if (email && !isWorkEmail) {
    document.getElementById('emailFactor101').classList.add('has-error');
    document.getElementById(
      'notWorkEmailFactor101Error'
    ).style.display =
      'block';
    isFormValid = false;
  } else {
    document.getElementById(
      'notWorkEmailFactor101Error'
    ).style.display =
      'none';
  }

  if (!company) {
    document.getElementById('companyFactor101').classList.add('has-error');
    document.getElementById('companyFactor101Error').style.display =
      'block';
    isFormValid = false;
  } else {
    document.getElementById('companyFactor101').classList.remove('has-error');
    document.getElementById('companyFactor101Error').style.display =
      'none';
  }

  if (!phone) {
    document.getElementById('phoneFactor101').classList.add('has-error');
    document.getElementById('phoneFactor101Error').style.display =
      'block';
    isFormValid = false;
  } else {
    document.getElementById('phoneFactor101').classList.remove('has-error');
    document.getElementById('phoneFactor101Error').style.display =
      'none';
  }

  return isFormValid;
}
