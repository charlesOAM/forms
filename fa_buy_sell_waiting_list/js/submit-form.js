function submitFactor101Form(event) {
  event.preventDefault();
	var elts =  document.querySelector('.factor101-form');

	// validation ... are required fields filled out?
	if (!validateFactor101SignupForm()) {
		return false;
	}


	var factor101Form = document.getElementById('factor101-form');
  factor101Form.action="https://www.getdrip.com/forms/435143005/submissions";
	factor101Form.method = "post";
  factor101Form.submit();
}
